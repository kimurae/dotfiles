# dotfiles
This manages homebrew packages and 'dot' file configuration for my setup.

## Getting started
```
sh -c "$(curl -fsLS get.chezmoi.io)" -- init --apply kimurae
```
